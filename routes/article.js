const express = require('express');
const router = express.Router();
const articleCtrl = require('../controllers/article');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');


router.get('/', articleCtrl.getAllArticles);
router.get('/:id', articleCtrl.getArticleById);
router.put('/:id', auth, multer, articleCtrl.updateArticle);
router.post('/add', auth, multer,  articleCtrl.addArticle);
router.delete('/:id', auth, articleCtrl.deleteArticle);


module.exports = router;