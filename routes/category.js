const express = require('express');
const router = express.Router();
const categoryCtrl = require('../controllers/category');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

router.post('/add', auth, multer,  categoryCtrl.addCategory);
router.get('/', categoryCtrl.getAllCategorys);
router.get('/:id', categoryCtrl.getCategoryById);
router.put('/:id', auth, multer, categoryCtrl.updateCategory);
router.delete('/:id', auth, categoryCtrl.deleteCategory);


module.exports = router;