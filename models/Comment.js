const mongoose = require('mongoose');
import { User } from '../models/user';


const commentSchema = mongoose.Schema({
    body: {type: String, require: true},
    date: {type: Date, require: true},
    state: {type: Boolean, require: true},
    user: {type: User, require: true}
});




module.exports = mongoose.model('Comment', commentSchema);