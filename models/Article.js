const mongoose = require('mongoose');
//import { Category } from '../models/Category';


const articleSchema = mongoose.Schema({
    title: {type: String, require: true},
    body: {type: String, require: true},
    date: {type: Date, require: true},
    thumbnail: {type: String, require: true},
    category: [
        { type : mongoose.Schema.Types.ObjectId, ref : 'Category' }
    ],
    comments: [
        { type : mongoose.Schema.Types.ObjectId, ref : 'Comment' }
    ]
});


module.exports = mongoose.model('Article', articleSchema);