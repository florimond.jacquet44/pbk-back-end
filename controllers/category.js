const Category = require('../models/Category');
const fs = require('fs');

//Ajouter un category
exports.addCategory = (req, res, next) => {
    const categ = {
        label: req.body.label
    }
    const category = new Category(categ);
    category.save().then(
        () => {
            res.status(201).json({message: 'La categorie à bien été ajoutée'});
        }
    ).catch((err) => {
        console.error(err);
        res.status(400).json({error: err});
    });
};

//Récupérer tous les categorys
exports.getAllCategorys = (req, res, next) => {
    Category.find().then(
        (categs) => {
            res.status(200).json(categs);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

//Récupérer un category par son ID
exports.getCategoryById = (req, res, next) => {
    Category.findOne({ _id: req.params.id }).then(
        (category) => {
            res.status(200).json(category);
        }
    ).catch(
        (error) => {
            console.error(error);
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.updateCategory = (req, res, next) => {
    const categoryObject = { ...req.body };
    Category.updateOne({_id: req.params.id}, categoryObject) .then(
        () => {
            res.status(201).json({message: 'La categorie a bien été modifiée'});
        }
    ).catch((err) => {
        console.error(err);
        res.status(400).json({error: err});
    });
};

exports.deleteCategory = (req, res, next) => {
    Category.findOne({ _id: req.params.id })
      .then(category => {
          Category.deleteOne({ _id: category._id })
            .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
            .catch(error => res.status(400).json({ error}));
        })
      .catch(error => res.status(500).json({ error }));
};