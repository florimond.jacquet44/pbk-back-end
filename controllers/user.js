const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10).then(
        (hash) => {
            const user = new User({
                pseudo: req.body.pseudo,
                email: req.body.email,
                password: hash
            });

            user.save().then(
                () => {
                    res.status(201).json({message: 'Utilisateur créé'});
                }
            ).catch((err) => {
                console.error(err);
                res.status(400).json({error: err});
            });
        }
    );
};

exports.login = (req, res, next) => {
    User.findOne({email: req.body.email, pseudo: req.body.pseudo}).then(
        function (user) {
            if (!user) {
                return res.status(401).json({ error: 'Utilisateur non trouvé !' });
            }
            bcrypt.compare(req.body.password, user.password).then(
                (valid) => {
                    if (!valid) {
                        return res.status(401).json({ error: 'Mot de passe incorrect !' });
                    }
                    const token = jwt.sign({ userId: user._id }, 'RANDOM_TOKEN_SECRET', { expiresIn: '24h' });
                    res.status(200).json({
                        userId: user._id,
                        token: token
                    });
                }
            ).catch((err) => {
                res.status(500).json({
                    error: error
                });
            });
        }
    ).catch((err) => {
        res.status(500).json({
            error: error
        });
    });
};

exports.getUser = (req, res, next) => {
    User.findOne({ _id: req.params.id }).then(
        (user) => {
            res.status(200).json(user);
        }
    ).catch(
        (error) => {
            console.error(error);
            res.status(400).json({
                error: error
            });
        }
    );
};