const Article = require('../models/Article');
const fs = require('fs');

//Ajouter un article
exports.addArticle = (req, res, next) => {
    console.log(req.body);
    const articleObject = JSON.parse(req.body.article)
    const article = new Article({
        ...articleObject, 
        thumbnail: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });
    article.save().then(
        () => {
            res.status(201).json({message: 'L\'article a bien été ajouté'});
        }
    ).catch((err) => {
        console.error(err);
        res.status(400).json({error: err});
    });
};

//Récupérer tous les articles
exports.getAllArticles = (req, res, next) => {
    Article.find().then(
        (things) => {
            res.status(200).json(things);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

//Récupérer un article par son ID
exports.getArticleById = (req, res, next) => {
    Article.findOne({ _id: req.params.id }).then(
        (article) => {
            res.status(200).json(article);
        }
    ).catch(
        (error) => {
            console.error(error);
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.updateArticle = (req, res, next) => {
    const articleObject = req.file ?
        {
        ...JSON.parse(req.body.article),
        thumbnail: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
        } : { ...req.body };
    

    Article.updateOne({_id: req.params.id}, { ...articleObject, _id: req.params.id }) .then(
        () => {
            res.status(201).json({message: 'L\'article a bien été modifié'});
        }
    ).catch((err) => {
        console.error(err);
        res.status(400).json({error: err});
    });

};

exports.deleteArticle = (req, res, next) => {
    Article.findOne({ _id: req.params.id })
      .then(article => {
        const filename = article.thumbnail.split('/images/')[1];
        fs.unlink(`images/${filename}`, () => {
          Article.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
            .catch(error => res.status(400).json({ error}));
        });
      })
      .catch(error => res.status(500).json({ error }));
};
