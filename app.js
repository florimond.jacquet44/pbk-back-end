//Injection des dépendances
const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
//Appel de EXPRESS
const app = express();
app.use(require('body-parser').urlencoded({extended: false}));

const router = express.Router();

//Différentes routes d'accès
const userRoutes = require('./routes/user');
const articleRoutes = require('./routes/article');  
const categoryRoutes = require('./routes/category');

//Connexion à la base
mongoose.connect('mongodb+srv://finomlord:Thg258FJC;@cluster0-ibvgi.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));


//Parse en JSON pour les échanges URI
app.use(bodyparser.json());


//Spec de paramétrage
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

//ROUTES utilisateur
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/api/auth', userRoutes);
app.use('/api/article', articleRoutes);
app.use('/api/category', categoryRoutes);



//app.get('/home', function(req, res){ res.status(200);});


module.exports = app;